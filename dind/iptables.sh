#!/bin/sh
/sbin/iptables -N DOCKER-USER
/sbin/iptables -I DOCKER-USER 1 -m state --state ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -I DOCKER-USER 2 -i eth0 -j ACCEPT
#/sbin/iptables -t nat -N DOCKER
#/sbin/iptables -t nat -I PREROUTING 1 -i eth0 -d 172.16.2.2 -p udp -m udp --dport 53 -j DNAT --to-destination 127.0.0.11:53
#/sbin/iptables -t nat -A POSTROUTING ! -s 127.0.0.1 -p udp --dport 53 -j MASQUERADE
#/sbin/iptables -I DOCKER-USER 2 -i eth0 -j ACCEPT
