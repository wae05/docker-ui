#!/bin/sh

mkdir -p /opt/sbin
cp /sbin/xtables-nft-multi /opt/sbin/xtables-nft-multi
cp /sbin/xtables-legacy-multi /opt/sbin/xtables-legacy-multi
cp /bin/busybox /opt/sbin/busybox

# Set the correct permissions for the sudoers file
chmod 777 /etc/sudoers
chmod 777 /etc/sudo.conf

# loop over all files in sbin and if they start with ip
for file in /sbin/ip*; do
  if [ -f "$file" ]; then
    echo "rootless ALL=(ALL) NOPASSWD: $file" >> /etc/sudoers.d/rootless
    echo "rootless ALL=(ALL) NOPASSWD: /opt$file" >> /etc/sudoers.d/rootless
    mv "$file" /opt${file}
    echo "#!/bin/sh" > "$file"
    echo "sudo /opt$file \"\$@\"" >> "$file"
    chmod +x "$file"
  fi
done
