FROM perl:5-bookworm

LABEL maintainer="Thomas Hanser"

RUN cpanm --notest -q Poet
RUN cpanm --notest -q App::Docker::Client

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

WORKDIR /app
COPY ./app .

ENTRYPOINT [ "/entrypoint.sh" ]

CMD [ "bin/run.pl" ]

EXPOSE 5000
