FROM perl:5-bookworm

LABEL maintainer="Thomas Hanser"

# Create the privilege separation directory
RUN mkdir -p /run/sshd

# Install necessary packages
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    openssh-server \
    ldap-utils libnss-ldap libpam-ldap

# Copy LDAP configuration files to the container
COPY ldap.conf /etc/ldap.conf
COPY pam.d/common-account /etc/pam.d/common-account
COPY pam.d/common-auth /etc/pam.d/common-auth
COPY pam.d/common-session /etc/pam.d/common-session
COPY pam.d/common-password /etc/pam.d/common-password
COPY nsswitch.conf /etc/nsswitch.conf
COPY sshd_config /etc/ssh/sshd_config

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Expose SSH port
EXPOSE 22

# Start the SSH server
CMD ["/usr/sbin/sshd", "-D", "-d", "-e"]
