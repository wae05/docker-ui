# Web Application Engineering and Design

## Project Group 5: CMS

### Members

- [Thomas Hanser]()
- []()

## Init Project

```bash
docker build -t wea-poet .
docker run -it --rm -v $(pwd):/src wea-poet poet new APP_NAME
```

## Project Outline

- multiple users in different groups
- users can update their poet instances via ssh (cpan module management)
- volumes for cpan modules and html/mason files
- nginx reverse proxy for multiple instances via dynamic path rewrites
- ssh jump host for container access

- admin interface for user management
  - add/remove users
  - add/remove groups
  - start/stop/purge/delete old instances

- user interface for container mgmt
  - start/stop instances
  - upload/download/rotate ssh credentials
  - get ssh connection string

### LDAP model:

- users
  - name/password
  - ssh keys
- groups/instance
  - name
  - database
  - memory/cpu limits
- users_groups
  - user_id
  - group_id

### Docker container model:

- volumes
  - cpan modules
  - html/mason files
- environment variables
  - GID -> group id to use for LDAP authorization

