<%class>
</%class>

<%init>
$.title('Docker Containers');
my $containers = $.docker->get('/containers/json');
</%init>

<h2>Running Docker Containers</h2>

<ul>
% foreach my $container ( @{$containers} ) {
  <li><% $container->{Id} %>: <% $container->{State} %></li>
% }
</ul>
