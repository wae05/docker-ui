<%class>
</%class>

<header>
    <h1><% $m->defer(sub { $m->page->title }) %></h1>
    <nav>
        <a href="/">Home</a> |
        <a href="/containers">Containers</a> |
        <a href="/images">Images</a>
    </nav>
</header>
