<%class>
</%class>

<%init>
$.title('Docker Images');
my $images = $.docker->get('/images/json');
</%init>

<h2>Available Container Images</h2>

<ul>
% foreach my $image ( @{$images} ) {
  <li><% $image->{Id} %>: <% $image->{Size} %></li>
% }
</ul>
