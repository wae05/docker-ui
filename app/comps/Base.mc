<%class>
has 'title'  => ( default => 'My site' );
has 'docker' => ( is => 'ro', default => sub { App::Docker::Client->new() } );
</%class>

<%init>
use App::Docker::Client;
</%init>

<%augment wrap>
  <html>
    <head>
      <link rel="stylesheet" href="/static/css/style.css">
      <title><% $m->defer(sub { $m->page->title }) %></title>
    </head>
    <body>
      <% $m->scomp('/header.mc') %>
        <% inner() %>
      <% $m->scomp('/footer.mc') %>
    </body>
  </html>
</%augment>
