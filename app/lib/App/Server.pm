package App::Server;
use Poet qw($conf $poet);
use Poet::Moose;
extends 'Poet::Server';

# Add customizations to Poet::Conf here.
#
# e.g. Use INI instead of YAML for config files
#
# use Config::INI;
# override 'read_conf_file' => sub {
#     my ($self, $file) = @_;
#     return Config::INI::Reader->read_file($file);
# };

override 'get_plackup_options' => sub {
    
    my @options = super();

    if ( $conf->is_development ) {
        # In development mode, also reload server when comps_dir changes
        push( @options, '-R', join( ",", $poet->comps_dir,) );
    }

    return @options;
};

1;
